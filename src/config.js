import Firebase from 'firebase';
import firebaseConfig from '../firebase.json';

let app = Firebase.initializeApp(firebaseConfig);

export const db = app.database();