import React, { Component } from 'react';
import { StyleSheet, Button, View, Text } from 'react-native';
import firebase from 'firebase';

class Home extends Component {
  
  state = { currentUser: null }

  componentDidMount() {
    const { currentUser } = firebase.auth()
    this.setState({ currentUser })
  }

  render() {
    const { currentUser } = this.state
    return (
      <View style={styles.container}>
        <Text>Home Screen</Text>
        {
          currentUser &&
          <View>
            <Button
              title="Add an Item"
              onPress={() => this.props.navigation.navigate('AddItem')}
            />
            <Button
              title="List of Items"
              color="green"
              onPress={() => this.props.navigation.navigate('List')}
            />
          </View>
        }
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center'
  }
})

export default Home;