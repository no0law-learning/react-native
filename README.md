# react-native

react-native is a react native application destined for learning the basics of react native.

## Installation

Must have:
* [npm/node](https://nodejs.org/en/)

```bash
npm install -g expo-cli
expo init <application_name>
cd <application_name>
npm run start
```

## Before Running

Add `firebase.config` on root with the following:
```json
{
    "apiKey": "apiKey",
    "authDomain": "authDomain",
    "databaseURL": "databaseURL",
    "projectId": "projectId",
    "storageBucket": "storageBucket",
    "messagingSenderId": "messagingSenderId",
    "appId": "appId"
}
```

## Useful Links

* [Integrating Firebase with React Native](https://blog.jscrambler.com/integrating-firebase-with-react-native/)